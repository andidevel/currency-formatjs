/**
CurrencyInputFormat
Copyright (C) 2021  Anderson R. Livramento <catfishlabs.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Version 0.0.1
**/

class CurrencyInputFormat {
    constructor (el, lang, precision) {
        this.lang = lang;
        this.precision = precision;
        this.el = [];
        this.numberFmt = new Intl.NumberFormat(
            lang,
            {
                minimumFractionDigits: precision
            }
        );
        if (typeof el === 'string') {
            this.el = document.querySelectorAll(el);
        }
        else {
            this.el = [el]
        }
        for (let i=0; i < this.el.length; i++){
            let sValue = this.el[i].value;
            let value = 0.0;
            if (sValue) {
                value = parseFloat(sValue);
                if (isNaN(value)) {
                    value = 0.0;
                }
            }
            this.el[i].value = this.numberFmt.format(value);
            this.el[i].onkeydown = (event) => {
                this.currencyKeydown(event);
            }
        }
    }

    currencyKeydown(event) {
        let delete_keys = ['Backspace', 'Delete'];
        if (delete_keys.includes(event.code)){
            // Perform a delete from end of value
            let s = this.removeFormat(event.currentTarget.value)
            const valueSize = s.length;
            s = s.slice(0, valueSize-1);
            event.currentTarget.value = this.numberFmt.format(this.toFloat(s));
        }
        else {
            if (/[0-9]/.test(event.key)){
                let s = event.currentTarget.value + event.key;
                event.currentTarget.value = this.numberFmt.format(this.toFloat(s));
            }
        }    
        event.preventDefault();
    }

    removeFormat(s) {
        const commonTokens = [',', '.'];
        let result = s;
        for (let i=0; i < commonTokens.length; i++) {
            result = result.replaceAll(commonTokens[i], '');
        }
        return result;
    }

    toFloat(s) {
        let result = this.removeFormat(s);
        const invPrecision = this.precision * -1;
        let floatResult = parseFloat(result.slice(0, result.length+invPrecision) + '.' + result.slice(invPrecision));
        if (isNaN(floatResult)) {
            floatResult = 0.0;
        }
        return floatResult;
    }

}
